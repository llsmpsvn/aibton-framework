#aibton-framework
- aibton-framework是一款规范开发流程的框架，系统核心分为如下模块
    1. aibton-core    框架核心，主要包括运行的工具类和主要的基类

#具体功能描述
- 该框架适用于ajax异步请求调用，不适用于服务器需要控制页面路由
- 核心功能：
    1. 统一的网关调用
    2. 统一的服务层写法，request、response格式统一
    3. 实现了基于注解的参数校验
    4. 实现了基于API的权限控制
    5. 实现了系统的统一异常处理
    6. 基本工具类的实现
- 框架异常采用RequestException自定义异常抛出用户可以自己按照框架定义解析
异常一般情况下抛出枚举信息如下定义
```
public interface IEnum {

    /**
     * 获取code值
     * @return
     */
    String getCode();

    /**
     * 获取value值
     * @return
     */
    String getValue();

    /**
     * 获取分组
     * @return
     */
    String getGroup();

    /**
     * 获得枚举编码
     * @return
     */
    String getCode(String value, String group);

    /**
     * 获得枚举值
     * @return
     */
    String getValue(String code, String group);

    default String getName() {
        return ((Enum) this).name();
    }
}
```

对应的Code值和msg值在AibtonConstantKey.class中如下：

```
public class AibtonConstantKey {

    /**
     * 系统正常返回code
     */
    public static final String RESPONSE_000000       = "000000";

    /**
     * 用户没有权限访问code
     */
    public static final String RESPONSE_400000       = "400000";

    /**
     * 系统内部异常
     */
    public static final String EXCEPTION_OF_MESSAGE  = "系统内部异常";

    /**
     * SYSTEM
     */
    public static final String SYSTEM                = "system";

    /**
     * JSON对象转换异常
     */
    public static final String SYSTEM_JACK_SON_ERROR = "JSON对象转换异常";

    /**
     * 用户没有权限访问该接口
     */
    public static final String USER_NOT_AUTH_ERROR   = "用户没有权限访问该接口";

    /**
     * HTTP请求调用异常
     */
    public static final String HTTP_ERROR            = "HTTP请求调用异常";

    /**
     * token值
     */
    public static final String TOKEN                 = "token";
}
```

###开始使用
- 基础配置
    - 基于注解的配置
```
@Configuration
public class AibtonConfig {

    @Bean
    public IApiEngine getIApiEngine() {
        return new ApiEngineImpl();
    }

    @Bean
    public ApiInitProcessor getApiInitProcessor() {
        return new ApiInitProcessor();
    }
}
```
    - 如果你使用xml，改成对应的xml配置即可


- 基本功能使用例子--只使用基本api访问功能
```
@Service
@Transactional
@Api(value = "systemInfo")
@Auth(auth = { "BASE", "ADMIN" })
public class SystemInfoApi extends AbstractBaseApi<BaseRequest, SystemInfoResponse> {

    @Override
    public BaseResponse excute(BaseRequest request, SystemInfoResponse response) {
        return ResponseUtils.getData(true, "");
    }
}
```

- 网关Controller写法
```
@RestController
public class GatewayController {

    private static final Logger LOGGER = LoggerFactory.getLogger(GatewayController.class);

    @Autowired
    private IApiEngine          iApiEngine;

    @RequestMapping(value = "gateway")
    public Object doExcute(HttpServletRequest request, String api, String data) {
        AssertUtils.isNotEmpty(LOGGER, api, "");
        AssertUtils.isNotEmpty(LOGGER, data, "");
        EngineContext engineContext = new EngineContext();
        engineContext.setApiUrl(api);
        engineContext.setRequestData(data);
        engineContext.setRequest(request);
        BaseResponse baseResponse = iApiEngine.run(engineContext);
        System.out.println(baseResponse);
        return baseResponse;
    }
}
```
以上则是最基本的功能的使用

### 参数校验功能写法

```
public class SystemInfoResponse extends BaseResponse {

    @NotEmpty
    private String desc;

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
```
所有的入参和返回参数都需要继承框架一个基类，如果是Request则继承BaseRequest，如果是Response则继承BaseResponse

框架现在实现了三个参数校验
```
@NotEmpty 字符串不能为NULL或者""
@NotNull 对象不能为NULL
@Length 字符串的长度限制
```
只要在request中参数有上面的注解，框架就会自动校验
### API权限使用
- 描述
    用户可以自定义用户权限来操作对应的API
- 使用
```
@Auth(auth = {"BASE","ADMIN"})//使用在API类名上
```
需要在每次访问网关时候设置一下用户拥有的权限列表，框架把权限值放在ThreadLocal中的
```
ApiThreadLocalUtils.setAuthDatas(null);//null就是你的权限列表
```
按照上面配置好，框架自动校验，以上则更新到这里啦

    
