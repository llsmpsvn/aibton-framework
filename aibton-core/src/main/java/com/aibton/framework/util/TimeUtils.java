/**
 * Aibton.com Inc.
 * Copyright (c) 2004-2017 All Rights Reserved.
 */
package com.aibton.framework.util;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 时间工具类
 * @author huzhihui
 * @version v 0.1 2017/5/9 22:06 huzhihui Exp $$
 */
public class TimeUtils {

    private static Logger LOGGER = LoggerFactory.getLogger(TimeUtils.class);

    public static String getUUID() {
        return UUID.randomUUID().toString();
    }

    /**
     * 获取当前时间
     * yyyy-MM-dd HH:mm:ss
     * @return  yyyy-MM-dd HH:mm:ss
     */
    public static String getNowTime() {
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");// 设置日期格式
        return df.format(new Date());
    }

    /**
     * 获取当前日期
     * yyyy-MM-dd
     * @return  yyyy-MM-dd
     */
    public static String getNowDate() {
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");// 设置日期格式
        return df.format(new Date());
    }

    /**
     * 获取当前时间到毫秒数
     * yyyyMMddHHmmssSSS
     * @return  yyyyMMddHHmmssSSS
     */
    public static String getCurrentMillisTime() {
        try {
            Thread.sleep(1);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        SimpleDateFormat df = new SimpleDateFormat("yyyyMMddHHmmssSSS");// 设置日期格式
        return df.format(new Date());
    }
}
