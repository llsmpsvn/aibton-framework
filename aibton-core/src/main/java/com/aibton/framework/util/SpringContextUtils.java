/**
 * Aibton.com Inc.
 * Copyright (c) 2016-2017 All Rights Reserved.
 */
package com.aibton.framework.util;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

/**
 * bean获取工具类
 * @author huzhihui
 * @version $: v 0.1 2017 2017/7/19 16:08 huzhihui Exp $$
 */
public class SpringContextUtils implements ApplicationContextAware {

    private static ApplicationContext applicationContext;

    @Override
    public void setApplicationContext(ApplicationContext arg0) throws BeansException {
        SpringContextUtils.applicationContext = arg0;
    }

    /**
     * 得到applicationContext 对象
     * @return  ApplicationContext
     */
    public static ApplicationContext getApplicationContext() {
        return applicationContext;
    }

    /**
     * 执行查找bean
     * @param beanName  beanName
     * @return  返回查询到的bean实例
     */
    public static Object getBean(String beanName) {
        return applicationContext.getBean(beanName);
    }
}
