/**
 * Aibton.com Inc.
 * Copyright (c) 2016-2017 All Rights Reserved.
 */
package com.aibton.framework.api.data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.aibton.framework.api.AbstractBaseApi;
import com.aibton.framework.api.Interceptor.IBaseApiInterceptor;
import com.aibton.framework.api.handel.*;
import com.aibton.framework.data.BaseRequest;
import com.aibton.framework.data.BaseResponse;

/**
 * API执行引擎上下文bean
 * @author huzhihui
 * @version $: v 0.1 2017 2017/7/20 15:30 huzhihui Exp $$
 */
public class EngineContext implements Serializable {

    /**
     * 对应Api注解名称
     */
    private String                    apiUrl;

    /**
     * Api执行具体类
     */
    private AbstractBaseApi           abstractBaseApi;

    /**
     * 输入参数
     */
    private BaseRequest               baseRequest;

    /**
     * 输入参数String
     */
    private String                    requestData;

    /**
     * HttpServletRequest
     */
    private HttpServletRequest        request;

    /**
     * 返回参数
     */
    private BaseResponse              baseResponse;

    /**
     * 拦截器列表
     */
    private List<IBaseApiInterceptor> apiInterceptors;

    /**
     * Handel处理器
     */
    private List<IBaseApiHandel>      apiHandels;

    /**
     * 后置拦截器列表
     */
    private List<IBaseApiInterceptor> afterApiInterceptors;

    /**
     * 后置Handel处理器
     */
    private List<IBaseApiHandel>      afterApiHandels;

    public EngineContext() {
        List<IBaseApiHandel> apiHandels = new ArrayList<>();
        apiHandels.add(new ApiSetHandel());
        apiHandels.add(new AibtonAuthValidateHandel());
        apiHandels.add(new RequestDataHandel());
        apiHandels.add(new ResponseDataInitHandel());
        apiHandels.add(new RequestDataValidateHandel());
        this.apiHandels = apiHandels;
        List<IBaseApiHandel> afterApiHandels = new ArrayList<>();
        afterApiHandels.add(new ResponseDataHandel());
        this.afterApiHandels = afterApiHandels;
    }

    public String getApiUrl() {
        return apiUrl;
    }

    public void setApiUrl(String apiUrl) {
        this.apiUrl = apiUrl;
    }

    public BaseRequest getBaseRequest() {
        return baseRequest;
    }

    public void setBaseRequest(BaseRequest baseRequest) {
        this.baseRequest = baseRequest;
    }

    public BaseResponse getBaseResponse() {
        return baseResponse;
    }

    public void setBaseResponse(BaseResponse baseResponse) {
        this.baseResponse = baseResponse;
    }

    public List<IBaseApiInterceptor> getApiInterceptors() {
        return apiInterceptors;
    }

    public AbstractBaseApi getAbstractBaseApi() {
        return abstractBaseApi;
    }

    public void setAbstractBaseApi(AbstractBaseApi abstractBaseApi) {
        this.abstractBaseApi = abstractBaseApi;
    }

    public List<IBaseApiHandel> getApiHandels() {
        return apiHandels;
    }

    public String getRequestData() {
        return requestData;
    }

    public void setRequestData(String requestData) {
        this.requestData = requestData;
    }

    public void setApiInterceptors(List<IBaseApiInterceptor> apiInterceptors) {
        this.apiInterceptors = apiInterceptors;
    }

    public void setApiHandels(List<IBaseApiHandel> apiHandels) {
        this.apiHandels = apiHandels;
    }

    public List<IBaseApiInterceptor> getAfterApiInterceptors() {
        return afterApiInterceptors;
    }

    public void setAfterApiInterceptors(List<IBaseApiInterceptor> afterApiInterceptors) {
        this.afterApiInterceptors = afterApiInterceptors;
    }

    public List<IBaseApiHandel> getAfterApiHandels() {
        return afterApiHandels;
    }

    public void setAfterApiHandels(List<IBaseApiHandel> afterApiHandels) {
        this.afterApiHandels = afterApiHandels;
    }

    public HttpServletRequest getRequest() {
        return request;
    }

    public void setRequest(HttpServletRequest request) {
        this.request = request;
    }
}
