/**
 * Aibton.com Inc.
 * Copyright (c) 2016-2017 All Rights Reserved.
 */
package com.aibton.framework.api.threadLocal;

import java.util.List;

import com.aibton.framework.api.Interceptor.IBaseApiInterceptor;
import com.aibton.framework.api.auth.data.AuthData;
import com.aibton.framework.api.handel.IBaseApiHandel;
import com.aibton.framework.data.BaseResponse;

/**
 * Api 当前线程副本
 * @author huzhihui
 * @version $: v 0.1 2017 2017/7/24 15:58 huzhihui Exp $$
 */
public class ApiThreadLocalUtils {

    /**
     * 用户自定义API拦截器
     */
    private static ThreadLocal<List<IBaseApiInterceptor>> iBaseApiInterceptorThreadLocal = new ThreadLocal<>();

    /**
     * 用户自定义API handel处理器
     */
    private static ThreadLocal<List<IBaseApiHandel>>      iBaseApiHandelThreadLocal      = new ThreadLocal<>();

    private static ThreadLocal<List<AuthData>>            authDatas                      = new ThreadLocal<>();

    private static ThreadLocal<BaseResponse>              baseResponseThreadLocal        = new ThreadLocal<>();

    /**
     * 增加一个拦截器
     * @param iBaseApiInterceptor   拦截器对象
     */
    public static void addIBaseApiInterceptor(IBaseApiInterceptor iBaseApiInterceptor) {
        iBaseApiInterceptorThreadLocal.get().add(iBaseApiInterceptor);
    }

    /**
     * 增加一个处理器
     * @param iBaseApiHandel    处理器对象
     */
    public static void addIBaseApiHandel(IBaseApiHandel iBaseApiHandel) {
        iBaseApiHandelThreadLocal.get().add(iBaseApiHandel);
    }

    /**
     * 获取用户设置的拦截器
     * @return  用户设置的拦截器
     */
    public static List<IBaseApiInterceptor> getIBaseApiInterceptors() {
        return iBaseApiInterceptorThreadLocal.get();
    }

    /**
     * 获取用户设置的处理器
     * @return  用户设置的处理器
     */
    public static List<IBaseApiHandel> getIBaseApiHandels() {
        return iBaseApiHandelThreadLocal.get();
    }

    /**
     * 获取权限列表
     * @return  权限列表值
     */
    public static List<AuthData> getAuthDatas() {
        return authDatas.get();
    }

    /**
     * 设置权限列表
     * @param authDatas 权限列表对象
     */
    public static void setAuthDatas(List<AuthData> authDatas) {
        ApiThreadLocalUtils.authDatas.set(authDatas);
    }

    /**
     * 设置API处理器返回值
     * @param baseResponse  最终返回值
     */
    public static void setBaseResponse(BaseResponse baseResponse) {
        baseResponseThreadLocal.set(baseResponse);
    }

    /**
     * 获取API处理器返回值
     * @return  结果返回值
     */
    public static BaseResponse getBaseResponse() {
        return baseResponseThreadLocal.get();
    }
}
