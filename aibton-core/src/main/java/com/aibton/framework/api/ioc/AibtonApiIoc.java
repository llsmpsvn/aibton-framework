/**
 * Aibton.com Inc.
 * Copyright (c) 2016-2017 All Rights Reserved.
 */
package com.aibton.framework.api.ioc;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import com.aibton.framework.api.AbstractBaseApi;

/**
 * API注解的bean对应的容器
 * @author huzhihui
 * @version $: v 0.1 2017 2017/7/20 18:23 huzhihui Exp $$
 */
public class AibtonApiIoc {

    private static final Map<String, AbstractBaseApi> apiIoc = new ConcurrentHashMap<>();

    /**
     * 增加apiBean
     * @param apiName   api路径名称
     * @param apiBean   api定义bean
     */
    public static void addApiBean(String apiName, AbstractBaseApi apiBean) {
        apiIoc.put(apiName, apiBean);
    }

    /**
     * 通过apiName获取到apiBean
     * @param apiName   api路径名称
     * @return  api bean
     */
    public static AbstractBaseApi getApiBean(String apiName) {
        return apiIoc.get(apiName);
    }

}
