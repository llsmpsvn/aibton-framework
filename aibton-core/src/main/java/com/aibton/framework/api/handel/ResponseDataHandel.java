/**
 * Aibton.com Inc.
 * Copyright (c) 2016-2017 All Rights Reserved.
 */
package com.aibton.framework.api.handel;

import com.aibton.framework.api.data.EngineContext;
import com.aibton.framework.api.threadLocal.ApiThreadLocalUtils;

/**
 * 返回值处理器
 * @author huzhihui
 * @version $: v 0.1 2017 2017/7/28 15:07 huzhihui Exp $$
 */
public class ResponseDataHandel implements IBaseApiHandel {
    @Override
    public void doHandel(EngineContext engineContext) {
        /**
         * 返回值不为空才进行设置返回值
         */
        if (null != ApiThreadLocalUtils.getBaseResponse()) {
            engineContext.setBaseResponse(ApiThreadLocalUtils.getBaseResponse());
        }
    }
}
