/**
 * Aibton.com Inc.
 * Copyright (c) 2004-2017 All Rights Reserved.
 */
package com.aibton.framework.validate.annotation;

import java.lang.annotation.*;

/**
 * 不为null校验
 * @author huzhihui
 * @version v 0.1 2017/5/11 22:36 huzhihui Exp $$
 */
@Retention(RetentionPolicy.RUNTIME) // 注解会在class字节码文件中存在，在运行时可以通过反射获取到
@Target({ ElementType.FIELD, ElementType.METHOD }) //定义注解的作用目标**作用范围字段、枚举的常量/方法
@Documented //说明该注解将被包含在javadoc中
public @interface Length {

    /**
     * 最小长度
     * @return  value
     */
    int min() default 0;

    /**
     * 最大长度
     * @return  value
     */
    int max() default Integer.MAX_VALUE;

    /**
     * 提示信息
     * @return  错误提示信息
     */
    String message() default "字段长度不符合要求";
}
