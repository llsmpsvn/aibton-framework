/**
 * Aibton.com Inc.
 * Copyright (c) 2004-2017 All Rights Reserved.
 */
package com.aibton.framework.enums.inter;

/**
 * 系统配置枚举基类
 * 使用方式：枚举类需要实现IEnum接口，按照EnumConfigSimple写法
 * @author huzhihui
 * @version v 0.1 2017/5/9 22:23 huzhihui Exp $$
 */
public interface IEnum {

    /**
     * 获取code值
     * @return code值
     */
    String getCode();

    /**
     * 获取value值
     * @return  value值
     */
    String getValue();

    /**
     * 获取分组
     * @return  分组
     */
    String getGroup();

    /**
     * 获得枚举编码
     * @param value value
     * @param group group
     * @return  枚举编码
     */
    String getCode(String value, String group);

    /**
     * 获得枚举值
     * @param code  code
     * @param group group
     * @return  枚举值
     */
    String getValue(String code, String group);

    /**
     * 获取枚举名称
     * @return  获取枚举名称
     */
    default String getName() {
        return ((Enum) this).name();
    }
}
