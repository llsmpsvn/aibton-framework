/**
 * Aibton.com Inc.
 * Copyright (c) 2004-2017 All Rights Reserved.
 */
package com.aibton.framework.enums;

import com.aibton.framework.enums.inter.IEnum;

/**
 * 枚举配置类
 * @author huzhihui
 * @version v 0.1 2017/5/9 23:07 huzhihui Exp $$
 */
public enum EnumConfigSimple implements IEnum {
    ;

    private String code;

    private String value;

    private String group;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    EnumConfigSimple(String code, String value, String group) {
        this.code = code;
        this.value = value;
        this.group = group;
    }

    @Override
    public String getCode(String value, String group) {
        for (EnumConfigSimple e : EnumConfigSimple.values()) {
            if (e.getValue().equals(value) && e.getGroup().equals(group)) {
                return e.code;
            }
        }
        return null;
    }

    @Override
    public String getValue(String code, String group) {
        for (EnumConfigSimple e : EnumConfigSimple.values()) {
            if (e.getCode().equals(code) && e.getGroup().equals(group)) {
                return e.value;
            }
        }
        return null;
    }
}
