/**
 * Aibton.com Inc.
 * Copyright (c) 2004-2017 All Rights Reserved.
 */
package com.aibton.framework.exception;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StringUtils;

import com.aibton.framework.config.AibtonConstantKey;
import com.aibton.framework.enums.inter.IEnum;
import com.aibton.framework.util.ExceptionUtils;
import com.aibton.framework.util.ResponseUtils;

/**
 * 全局异常处理器
 * 使用方式：
 * @author huzhihui
 * @version v 0.1 2017/5/9 23:19 huzhihui Exp $$
 */
public abstract class ExceptionHandler {

    private static Logger logger = LoggerFactory.getLogger(ExceptionHandler.class);

    /**
     * 异常处理器--需要在实现类内部调用errorHandlerProcess即可
     * @param request   HttpServletRequest
     * @param e Exception
     * @return  异常返回值
     */
    public abstract Object errorHandler(HttpServletRequest request, Exception e);

    /**
     * 异常处理器具体实现方法
     * @param request   HttpServletRequest
     * @param e Exception
     * @return  异常返回对象
     */
    public Object errorHandlerProcess(HttpServletRequest request, Exception e) {
        logger.error("-----捕获服务器内部错误请求" + request.getRequestURI());
        logger.error("-----error:" + ExceptionUtils.getExceptionString(e));
        if (e instanceof RequestException) {
            RequestException requestException = (RequestException) e;
            IEnum iEnum = requestException.getiEnum();
            String exMsg = requestException.getExMsg();
            if (!StringUtils.isEmpty(exMsg)) {
                return ResponseUtils.getData(false, exMsg);
            }
            return ResponseUtils.getOtherData(false, iEnum.getCode(), iEnum.getValue());
        }
        return ResponseUtils.getData(false, AibtonConstantKey.EXCEPTION_OF_MESSAGE);
    }
}
